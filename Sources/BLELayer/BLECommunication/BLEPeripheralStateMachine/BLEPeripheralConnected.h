//
//  BLEPeripheralConnected.h
//  FlirNvrDvr
//
//  Created by Adaya on 3/23/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPeripheralConnectionState.h"

@interface BLEPeripheralConnected : NSObject<BLEPeripheralConnectionState>
- (instancetype)initWithPeripheral: (CBPeripheral *) peripheral andAvailableCharectaristics:(NSSet *)availableCharectaristics;

@end

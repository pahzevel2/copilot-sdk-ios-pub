//
//  DynamicElement.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class DynamicElement {
    
    private enum Keys {
        static let actionUrlKey = "actionUrl"
        static let auxiliaryTextKey = "auxiliaryText"
        static let descriptionKey = "description"
        static let imageUrlKey = "imageUrl"
        static let titleKey = "title"
    }
    
    public let actionUrl: String?
    public let auxiliaryText: String?
    public let description: String
    public let imageUrl: String?
    public let title: String
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let descriptionString = dictionary[Keys.descriptionKey] as? String,
            let titleString = dictionary[Keys.titleKey] as? String else {
                return nil
        }
        
        self.actionUrl = dictionary[Keys.actionUrlKey] as? String
        self.auxiliaryText = dictionary[Keys.auxiliaryTextKey] as? String
        self.description = descriptionString
        self.imageUrl = dictionary[Keys.imageUrlKey] as? String
        self.title = titleString
    }
}

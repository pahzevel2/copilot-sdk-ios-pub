//
//  ReferAFriendAPI.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 25/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class ReferAFriendAPI: ReferAFriendAPIAccess {
    
    typealias Dependencies = HasRafServiceInteraction & HasAuthenticationProvider
    private let dependencies: Dependencies

    //Fonts loaded in order to use them in raf view controller
    private let loadFonts: () = {
        UIFont.registerFontWith(filenameString: "SF-Pro-Text-Regular")
        UIFont.registerFontWith(filenameString: "SF-Pro-Text-Light")
        UIFont.registerFontWith(filenameString: "SF-Pro-Text-Bold")
        UIFont.registerFontWith(filenameString: "SF-Pro-Text-Medium")
        UIFont.registerFontWith(filenameString: "SF-Pro-Text-Semibold")
    }()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    public func getRafViewController() -> ReferAFriendViewController? {
        return CopilotViewControllersFactory.createControllerWithType(.mainRaf) as? ReferAFriendViewController
    }
    
    public func fetchRafData() -> RequestBuilder<RafData, FetchRafDataError> {
        return FetchRafDataRequestBuilder(dependencies: dependencies)
    }
    
    public func getGenerateCouponWorker(_ closure: @escaping RafPollingWorkerClosure<GenerateReferralCouponError>) -> RafPollingWorker<GenerateReferralCouponError> {
        let jobCreator = GenerateCouponJobCreator(dependencies: dependencies)
        return RafPollingWorker(dependencies: dependencies, jobCreator: jobCreator, closure: closure)
    }
    
    public func getClaimRewardWorker(withRewardsIds rewardsIds: [String], _ closure: @escaping RafPollingWorkerClosure<ClaimCreditError>) -> RafPollingWorker<ClaimCreditError> {
        let jobCreator = ClaimRewardJobCreator(rewardsIds: rewardsIds, dependencies: dependencies)
        return RafPollingWorker(dependencies: dependencies, jobCreator: jobCreator, closure: closure)
    }
    
    public func getPendingJobsRecoveryWorker(withPendingRafJobs pendingRafJobs: [RafJob], _ closure: @escaping JobsRecoveryWorkerClosure) -> JobsRecoveryWorker {
        return JobsRecoveryWorker(dependencies: dependencies, pendingRafJobs: pendingRafJobs, closure: closure)
    }
}



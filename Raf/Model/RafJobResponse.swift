//
//  RafJobResponse.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class RafJobResponse {
    
    private enum Keys {
        static let createdAtKey = "createdAt"
        static let idKey = "id"
        static let statusKey = "status"
        static let typeKey = "type"
        static let updatedAtKey = "updatedAt"
    }
    
    public enum JobStatus: String {
        case inProgress = "InProgress"
        case success = "Success"
        case failure = "Failure"
    }
    
    public enum JobType: String {
        case generateCoupon = "GenerateCoupon"
        case generateReward = "GenerateReward"
    }
    
    public let createdAt: Double?
    public let id: String
    public let status: JobStatus
    public let type: JobType
    public let updatedAt: Double?
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let id = dictionary[Keys.idKey] as? String,
            let statusString = dictionary[Keys.statusKey] as? String,
            let status = JobStatus(rawValue: statusString),
            let typeString = dictionary[Keys.typeKey] as? String,
            let type = JobType(rawValue: typeString) else {
                return nil
        }
        
        self.createdAt = dictionary[Keys.createdAtKey] as? Double
        self.id = id
        self.status = status
        self.type = type
        self.updatedAt = dictionary[Keys.updatedAtKey] as? Double
    }
}

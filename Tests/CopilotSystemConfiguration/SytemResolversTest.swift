//
//  SytemResolversTest.swift
//  CopilotAPIAccessTests
//
//  Created by Adaya on 05/03/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import XCTest
@testable import CopilotAPIAccess

class FetchConfigurationErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = FetchConfigurationError
    typealias R = FetchConfigurationErrorResolver
    
    let msg = "msg"
    
    func resolver() -> FetchConfigurationErrorResolver {
        return FetchConfigurationErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
}

class CheckAppVersionStatusErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = CheckAppVersionStatusError
    typealias R = CheckAppVersionStatusErrorResolver
    
    let msg = "msg"
    
    func resolver() -> CheckAppVersionStatusErrorResolver {
        return CheckAppVersionStatusErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .appVersionBadFormat(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
}

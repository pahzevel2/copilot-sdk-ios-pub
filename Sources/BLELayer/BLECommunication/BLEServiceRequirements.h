//
//  BLERequirements.h
//  FlirNvrDvr
//
//  Created by Adaya on 3/24/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BLEServiceRequirements : NSObject

@property (nonatomic, strong, readonly) NSArray* charectaristicIds;
@property (nonatomic, strong, readonly) CBUUID* serviceId;

-(instancetype)initWithId:(NSString*) serviceId andCharechtaristics: (NSArray*) charectaristicIds;// array of CBUUID

@end

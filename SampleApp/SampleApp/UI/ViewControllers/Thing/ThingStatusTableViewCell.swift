//
//  ThingStatusTableViewCell.swift
//  SampleApp
//
//  Created by Revital Pisman on 16/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class ThingStatusTableViewCell: UITableViewCell {
    
    //MARK: - Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    //MARK: - Functions
    
    func set(name: String, value: String, time: String) {
        nameLabel.text = name
        valueLabel.text = value
        timeLabel.text = time
    }
    
}

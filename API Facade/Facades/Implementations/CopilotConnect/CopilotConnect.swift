//
//  CopilotConnect.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 28/05/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class CopilotConnect: CopilotConnectAccess {

    public let app: AppAPIAccess
    public let auth: AuthAPIAccess
    public let user: UserAPIAccess
    public let thing: ThingAPIAccess
    
    public var defaultAuthProvider: AuthenticationProvider {
        return dependencies.authenticationProvider
    }
    private let dependencies: ConnectDependencies
    
    
    private static func defaultDependencies(reporter: ReportAPIAccess, configurationProvider: ConfigurationProvider) -> ConnectDependencies {
        let authenticationServiceInteraction = AuthenticationServiceInteraction()
        let userServiceInteraction = UserServiceInteraction(authenticationProvider: authenticationServiceInteraction, configurationProvider: configurationProvider)
        let configurationServiceInteraction = SystemConfigurationServiceInteraction()
        let thingsServiceInteraction = ThingsServiceInteraction(authenticationProvider: authenticationServiceInteraction)
        
        return ConnectDependencies(authenticationServiceInteraction: authenticationServiceInteraction,
                                   authenticationProvider: authenticationServiceInteraction,
                                   userServiceInteraction: userServiceInteraction,
                                   configurationServiceInteraction: configurationServiceInteraction,
                                   thingsServiceInteraction: thingsServiceInteraction,
                                   reporter: reporter,
                                   configurationProvider: configurationProvider
        )
    }
    
    init(authenticationProviderContainer: AuthenticationProviderContainer?, reporter: ReportAPIAccess, configurationProvider: ConfigurationProvider) {
        let dependencies = CopilotConnect.defaultDependencies(reporter: reporter, configurationProvider: configurationProvider)
        self.dependencies = dependencies
        
        app = ApplicationAPI(dependencies: dependencies)
        auth = AuthAPI(dependencies: dependencies)
        user = UserAPI(dependencies: dependencies)
        thing = ThingAPI(dependencies: dependencies)
        
        authenticationProviderContainer?.authenticationProvider = dependencies.authenticationProvider
    }
}

//
//  AuthErrorResolverTests.swift
//  CopilotAPIAccessTests
//
//  Created by Adaya on 28/02/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

import XCTest
@testable import CopilotAPIAccess

class LoginErrorResolverTests: XCTestCase, ResolverTest {
  
    typealias T = LoginError
    typealias R = LoginErrorResolver
    
    let msg = "msg"
   
    func resolver() -> LoginErrorResolver {
        return LoginErrorResolver()
    }

    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .unauthorized(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_MarkedForDeletion(){
        let error = resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .markedForDeletion(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func test_Unrecognized(){
         assertUnrecognized()
    }
    
    func customMarkedForDeletion() -> Bool {
        return true
    }
    
}

class SignupErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = SignupError
    typealias R = SignupErrorResolver
    
    let msg = "msg"
   
    func resolver() -> SignupErrorResolver {
        return SignupErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }

    func test_InvalidApplicationId(){
        let error = resolver().fromTypeSpecificError(TestConstants.invalidApplicationId, TestConstants.invalidApplicationIdReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .invalidApplicationId(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    func test_UserAlreadyExists(){
        let error = resolver().fromTypeSpecificError(TestConstants.userAlreadyExists, TestConstants.userAlreadyExistsReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .userAlreadyExists(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    func test_PasswordPolicyViolation(){
        let error = resolver().fromTypeSpecificError(TestConstants.passwordPolicyViolation, TestConstants.passwordPolicyViolationReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .passwordPolicyViolation(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func test_InvalidEmail(){
        let error = resolver().fromTypeSpecificError(TestConstants.invalidEmail, TestConstants.invalidEmailReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .invalidEmail(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customInvalidApplicationId() -> Bool {
        return true
    }
    
    func customPasswordPolicy() -> Bool {
        return true
    }
    
    func customUserAlreadyExists() -> Bool {
        return true
    }
    
    func customInvalidEmail() -> Bool {
        return true
    }
}

class ElevateAnonymousErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = ElevateAnonymousUserError
    typealias R = ElevateAnonymousErrorResolver
    
    let msg = "msg"
    
    func resolver() -> ElevateAnonymousErrorResolver {
        return ElevateAnonymousErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_UserAlreadyExists(){
        let error = resolver().fromTypeSpecificError(TestConstants.userAlreadyExists, TestConstants.userAlreadyExistsReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .userAlreadyExists(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    func test_PasswordPolicyViolation(){
        let error = resolver().fromTypeSpecificError(TestConstants.passwordPolicyViolation, TestConstants.passwordPolicyViolationReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .passwordPolicyViolation(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func test_CannotElevateExisting(){
        let error = resolver().fromTypeSpecificError(TestConstants.invalidPermissions, TestConstants.invalidPermissionsReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .cannotElevateANonAnonymousUser(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func test_MerkedForDeletion(){
        let error = resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .requiresRelogin(_):break
        default: XCTFail("Received \(error!)")
        }
    }
    func test_InvalidEmail(){
        let error = resolver().fromTypeSpecificError(TestConstants.invalidEmail, TestConstants.invalidEmailReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .invalidEmail(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
   
    
    func customPasswordPolicy() -> Bool {
        return true
    }
    
    func customUserAlreadyExists() -> Bool {
        return true
    }
    
    func customInvalidPermissions() -> Bool {
        return true
    }
    func customMarkedForDeletion() -> Bool {
        return true
    }
    func customInvalidEmail() -> Bool {
        return true
    }
}

class SignupAnonymousErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = SignupAnonymouslyError
    typealias R = SignupAnonymousErrorResolver
    
    let msg = "msg"
    
    func resolver() -> SignupAnonymousErrorResolver {
        return SignupAnonymousErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_InvalidApplicationId(){
        let error = resolver().fromTypeSpecificError(TestConstants.invalidApplicationId, TestConstants.invalidApplicationIdReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .invalidApplicationId(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
   
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customInvalidApplicationId() -> Bool {
        return true
    }
    
}


class FetchPasswordPolicyErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = FetchPasswordRulesPolicyError
    typealias R = FetchPasswordRulesPolicyErrorResolver
    
    let msg = "msg"
    
    func resolver() -> FetchPasswordRulesPolicyErrorResolver {
        return FetchPasswordRulesPolicyErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
}

class ApproveTermsOfUseErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = ApproveTermsOfUseError
    typealias R = ApproveTermsOfUseErrorResolver
    
    let msg = "msg"
    
    func resolver() -> ApproveTermsOfUseErrorResolver {
        return ApproveTermsOfUseErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customMarkedForDeletion() -> Bool {
        return true
    }
    
    func test_TranslateMarkForDeletion(){
        let error = resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .requiresRelogin(_):break
        default: XCTFail("Received \(error!)")
        }
    }
}

class ResetPasswordErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = ResetPasswordError
    typealias R = ResetPasswordErrorResolver
    
    let msg = "msg"
    
    func resolver() -> ResetPasswordErrorResolver {
        return ResetPasswordErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customEmailIsNotVerified() -> Bool {
        return true
    }
    func test_EmailIsNotVerified(){
        let error = resolver().fromTypeSpecificError(TestConstants.emailNotVerified, TestConstants.emailNotVerifiedReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .emailIsNotVerified(_):break
        default: XCTFail("Received \(error!)")
        }
    }
}

class RefreshTokenErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = RefreshTokenError
    typealias R = RefreshTokenErrorResolver
    
    let msg = "msg"
    
    func resolver() -> RefreshTokenErrorResolver {
        return RefreshTokenErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_TranslateMarkForDeletion(){
        let error = resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .requiresRelogin(_):break
        default: XCTFail("Received \(error!)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customMarkedForDeletion() -> Bool {
        return true
    }
}

class UpdateUserConsentErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = UpdateUserConsentError
    typealias R = UpdateUserConsentErrorResolver
    
    let msg = "msg"
    
    func resolver() -> UpdateUserConsentErrorResolver {
        return UpdateUserConsentErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customMarkedForDeletion() -> Bool {
        return true
    }
    
    func test_TranslateMarkForDeletion(){
        let error = resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .requiresRelogin(_):break
        default: XCTFail("Received \(error!)")
        }
    }
}

class ConsentRefusedErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = ConsentRefusedError
    typealias R = ConsentRefusedErrorResolver
    
    let msg = "msg"
    
    func resolver() -> ConsentRefusedErrorResolver {
        return ConsentRefusedErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
}

class ChangePasswordErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = ChangePasswordError
    typealias R = ChangePasswordErrorResolver
    
    let msg = "msg"
    
    func resolver() -> ChangePasswordErrorResolver {
        return ChangePasswordErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    func customResetPasswordFailed() -> Bool {
        return true
    }
    
    func test_ResetPasswordFailed(){
        let error = resolver().fromTypeSpecificError(TestConstants.fobidden, TestConstants.resetPasswordFailed, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .invalidCredentials(_):break
        default: XCTFail("Received \(error!)")
        }
    }
    func customPasswordPolicy() -> Bool {
        return true
    }
    
    func test_PassowrdPolicy(){
        let error = resolver().fromTypeSpecificError(TestConstants.passwordPolicyViolation, TestConstants.passwordPolicyViolationReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .passwordPolicyViolation(_):break
        default: XCTFail("Received \(error!)")
        }
    }
    func customMarkedForDeletion() -> Bool {
        return true
    }
    
    func test_TranslateMarkForDeletion(){
        let error = resolver().fromTypeSpecificError(TestConstants.markedForDeletion, TestConstants.markedForDeletionReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .requiresRelogin(_):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func customInvalidPermissions() -> Bool {
        return true
    }
    
    func test_InvalidPermissions(){
        let error = resolver().fromTypeSpecificError(TestConstants.invalidPermissions, TestConstants.invalidPermissionsReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .cannotChangePasswordToAnonymousUser(_):break
        default: XCTFail("Received \(error!)")
        }
    }
}

class SendVerificationEmailErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = SendVerificationEmailError
    typealias R = SendVerificationEmailErrorResolver
    
    let msg = "msg"
    
    func resolver() -> SendVerificationEmailErrorResolver {
        return SendVerificationEmailErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_Unrecognized(){
        assertUnrecognized()
    }
    func customUserAlreadyVerified() -> Bool {
        return true
    }
    
    func test_userAlraedyVerified(){
        let error = resolver().fromTypeSpecificError(TestConstants.userAlreadyVerified, TestConstants.userAlreadyVerifiedReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .userAlreadyVerified(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
}

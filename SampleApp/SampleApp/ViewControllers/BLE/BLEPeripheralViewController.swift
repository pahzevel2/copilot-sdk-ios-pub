//
//  BLEPeripheralViewController.swift
//  SampleApp
//
//  Created by Adaya on 12/01/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import UIKit
import ZemingoBLELayer

class BLEPeripheralViewController: UIViewController, PeripheralConnectionDelegate {
    
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var disconnectButton: UIButton!
    
    var bridge : BLEBridge?
    var peripheralInfo : PeripheralInfo?
    var peripheral : Peripheral?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let info = peripheralInfo{
            self.navigationItem.title = info.name
            connectButton.isEnabled = true
        }
        else{
            connectButton.isEnabled = false
        }
        disconnectButton.isEnabled = false
        self.navigationItem.backBarButtonItem?.title = "Disconnect"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let currentPeripheral = peripheral{
            currentPeripheral.disconnect()
        }
        peripheral = nil// we dont want to continue responding to events
    }
    
    @IBAction func connectPressed(_ sender: Any) {
        if peripheral != nil{
            return
        }
        if let bleBridge = bridge, let info = peripheralInfo{
            self.connectButton.isEnabled = false
            self.disconnectButton.isEnabled = true
            
            peripheral = bleBridge.connectToPeripheral(with:info, perhiperalConnectionDelegate: self)
        }
    }
    
    @IBAction func disconnectPressed(_ sender: Any) {
        if let currentPeripheral = peripheral{
            currentPeripheral.disconnect()
        }
    }
    
    func peripheralDidConnect(_ peripheral: Peripheral!) {
        DispatchQueue.main.async {
            self.disconnectButton.isEnabled = true
            if peripheral != self.peripheral{
                self.showAlert("Peripheral connected with different!")
            }
            else{
                self.showAlert("Peripheral - connection completed to: \(peripheral.peripheralInfo.name)")
            }
        }
    }
    public func peripheral(_ peripheral: Peripheral!, didDisconnectWithError error: Error!){
        if(peripheral != self.peripheral){
            return
        }
        bleDisconnected(error)
        
    }
    
    public func peripheral(_ peripheral: Peripheral!, connectionFailedWithError error: Error!){
        if(peripheral != self.peripheral){
            return
        }
        bleDisconnected(error)
    }
    
    func bleDisconnected(_ error : Error?){
        DispatchQueue.main.async {
            self.peripheral = nil
            self.disconnectButton.isEnabled = false
            self.connectButton.isEnabled = true
            if let err = error{
                self.showAlert("Disconnected with error \(err)")
            }
            else{
                self.showAlert("Succesfully disconnected")
            }
        }
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  BLEManagerErrors.h
//  ESC_BT_Proxy
//
//  Created by Elad Urson on 6/1/16.
//  Copyright © 2016 Elad Urson. All rights reserved.
//

#ifndef BLEManagerErrors_h
#define BLEManagerErrors_h

#define BLEManagerErrorDomainName         @"BLEManagerErrorDomain"

typedef enum {
    
    BLEManagerErrorPeripheralNotFound             = 98003,
    BLEManagerErrorServiceNotFound                = 98004,
    BLEManagerErrorCharectaristicNotFound         = 98005,
    BLEManagerErrorInvalidCurrentState            = 98008,
    BLEManagerErrorInvalidCurrentStateForReaponse = 98009,
    BLEManagerErrorInvalidPeripheralForResponse   = 98010,
    BLEManagerErrorUnexpectedDisconnection        = 98011
    
    
} BLEManagerError;


#define BLEPeripheralDiscovererErrorDomainName         @"BLEPeripheralDiscovererErrorDomain"
#define BLEManagerStatusErrorDomainName                @"BLEManagerStatusDomain"

typedef enum {
    
//    BLEManagerStatusErrorInvalidCurrentState                     = 99006,
    BLEManagerStatusErrorScanStoppedExternallyGeneral            = 99007,
    BLEManagerStatusErrorScanStoppedExternallyPowerOff           = 99008,
//    BLEManagerStatusErrorScanStoppedExternallyUnauthorized       = 99009,
    BLEManagerStatusErrorNotSupported                            = 99010,
    
} BLEManagerStatusError;

#endif /* BLEManagerErrors_h */

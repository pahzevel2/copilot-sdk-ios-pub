//
//  BLEPeripheralProtocol.h
//  FlirNvrDvr
//
//  Created by Elad Urson on 25/1/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//



@protocol BLEPeripheralDelegate <NSObject>

- (void)connectionSuccessToBLEPeripheral;
- (void)connectionFailedToBLEPeripheralWithError: (NSError *)error;


- (void)didDisconnectFromConnectedBLEPeripheralWithError:(NSError *)error;
- (void)didSetValueForCharacteristic:(NSString *)characteristicID withError:(NSError *)error;
- (void)didGetValue:(NSData *)value forCharacteristic:(NSString *)characteristicID withError:(NSError *)error;

@end



@protocol BLEPeripheralProtocol <NSObject>

@property (nonatomic, weak) id <BLEPeripheralDelegate> peripheralDelegate;

- (void) setValue:(NSData *)value forCharacteristic:(NSString *)characteristicID;
- (void) getValueforCharacteristic:(NSString *)characteristicID;
- (void) registerForCharacteristic:(NSString*) characteristicID;
- (void)unregisterForCharacteristic:(NSString *)characteristicID;
- (BOOL) disconnectFromBLEPeripheral;

@end

//
//  NSObject+String.swift
//  SampleApp
//
//  Created by Revital Pisman on 11/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

extension NSObject {
    class func stringFromClass() -> String {
        return String(describing: self)
    }
}

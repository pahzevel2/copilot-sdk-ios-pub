//
//  BLEStatusDelegateProtocol.h
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/18/17.
//  Copyright © 2017 Adaya. All rights reserved.
//

@protocol BLEStatusDelegate <NSObject>

-(void) onStatusChangedToConnected: (BOOL) connected orError: (NSError*) error;

@end

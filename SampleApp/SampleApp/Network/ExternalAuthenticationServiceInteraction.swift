//
//  ExternalAuthenticationServiceInteraction.swift
//  SampleAppDevIOTCO
//
//  Created by Revital Pisman on 20/06/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class AuthenticationServiceInteraction: CopilotTokenProvider {

    func generateUserAuthKey(for userId: String, withClosure closure: @escaping GenerateUserAuthKeyClosure) {
    
        RequestManager().generateAccessToken(userId: userId) { (response) in
            let generateAccessTokenResponse: Response<String, GenerateUserAuthKeyError>
            
            switch response {
            case .success(let authKey):
                generateAccessTokenResponse = .success((authKey))
                
            case .failure(let error):
                switch error {
                case .generalError:
                    generateAccessTokenResponse = .failure(error: .generalError(debugMessage: ""))
                case .connectivityError:
                    generateAccessTokenResponse = .failure(error: .connectivityError(debugMessage: ""))
                }
            }
            
            closure(generateAccessTokenResponse)
        }
        
    }
    
}

//
//  UIViewcontroller+AlertController.swift
//  SampleApp
//
//  Created by Revital Pisman on 24/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

public typealias PopupActionClosure = () -> ()

extension UIViewController {
    
    public func presentAlertController(cancelButtonText: String,  actionButtonText: String? = nil, cancelCompletionHandler: PopupActionClosure? = nil, actionCompletionHandler: PopupActionClosure? = nil) {
        let alertController = UIAlertController(title: "Unauthorized", message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: cancelButtonText, style: .cancel, handler: { (action) in
            cancelCompletionHandler?()
        })
        alertController.addAction(cancelAction)
        
        if let actionButtonText = actionButtonText {
            let action = UIAlertAction(title: actionButtonText, style: .default, handler: { (action) in
                actionCompletionHandler?()
            })
            alertController.addAction(action)
        }
        
        present(alertController, animated: true, completion: nil)
    }
}

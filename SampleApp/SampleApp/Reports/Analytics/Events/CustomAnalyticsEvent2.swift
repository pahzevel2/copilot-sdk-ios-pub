//
//  CustomAnalyticsEvent2.swift
//  SampleApp
//
//  Created by Tom Milberg on 09/04/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct CustomAnalyticsEvent2: AnalyticsEvent {
    
    let smartDeviceID: String

    var customParams: Dictionary<String, String> {
        return ["smartDeviceId" : smartDeviceID]
    }
    
    var eventName: String {
        return "Custom_Event_2"
    }
    
    var eventOrigin: AnalyticsEventOrigin {
        return .User
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.Main, .DevSupport, .All, .Engagement]
    }
}

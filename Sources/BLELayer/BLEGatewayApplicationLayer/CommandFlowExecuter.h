//
//  CommandFlowExecuter.h
//  ZemingoBLELayer
//
//  Created by Adaya on 19/01/2017.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BridgeCompletionBlocks.h"
#import "BLEPeripheralProtocol.h"
#import "CommandFlowProtocol.h"

@interface CommandFlowExecuter : NSObject

- (instancetype) initWithPeripheralProtocol:(id<BLEPeripheralProtocol>) peripheralProtocol;

- (void) didSetValueForCharacteristic:(NSString *)characteristicID withError:(NSError *)error;
- (void) didGetValue:(NSData *)value forCharacteristic:(NSString *)characteristicID withError:(NSError *)error;
- (void) addCommandFlow: (id<CommandFlowProtocol>) commandFlow;
- (void) interruptCommandFlow: (id<CommandFlowProtocol>) commandFlow;

@end

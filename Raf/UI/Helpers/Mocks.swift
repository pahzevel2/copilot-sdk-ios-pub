//
//  Mocks.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 10/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

//TODO: Should be removed before going on production
struct Mocks {
    
    static func generateDummyRafData(isAltruistic: Bool, isRafProgram: Bool, hasRewards: Bool, hasCredit: Bool, hasDynamicData: Bool) -> RafData {
        let rafProgram = !isRafProgram ? nil : RafProgram(bannerText: "This is banner text",
                                                          rewardCurrencySymbol: "$",
                                                          rewardValue: 20,
                                                          shareText: "Hey, enjoy using Levo with this coupon https://shop.levooil.com/13626597/checkouts/1558bb69f48cb6c588e1b7291d14df96?_ga=2.139473589.741692332.1565169785-1069281732.1565002538")
        
        let rafStaticData = StaticData(footerTitle: "This is footer title",
                                       storeName: "Levo",
                                       termsOfUseUrl: "https://www.levooil.com/terms-conditions/")
        
        let altruisticProgram = AltruisticProgram(description: "Altruism is the principle and moral practice of concern for happiness of other human beings and/or animals, resulting in a quality of life both material and spiritual. It is a traditional virtue in many cultures and a core aspect of various religious traditions and secular worldviews, though the concept of others toward whom concern should be directed can vary among cultures and religions. In an extreme case, altruism may become a synonym of selflessness which is the opposite of selfishness.",
                                                  shareText: "I'm using Levo, you should also! Buy Levo at : https://shop.levooil.com")
        
        var rafRewards = [Reward]()
        let numberOfRewards = Int.random(in: 0 ..< 5)
        
        //Add at least one active reward
        rafRewards.append(Reward(id: UUID().uuidString,
                                 status: .active,
                                 value: 10,
                                 currency: "€"))
        for _ in 0...numberOfRewards {
            let randomRewardType = RewardStatus.allCases.randomElement() ?? .active
            let randomRewardValue = (Int.random(in: 0 ..< 20) + 1) * 5
            rafRewards.append(Reward(id: UUID().uuidString,
                                     status: randomRewardType,
                                     value: Double(randomRewardValue),
                                     currency: "$"))
        }
        
        var rafDiscountCodes = [DiscountCode]()
        
        let numberOfDiscountCodes = Int.random(in: 0 ..< 5)
        for _ in 0...numberOfDiscountCodes {
            let randomString = generateRandomString(7)
            let randomInt = (Int.random(in: 0 ..< 20) + 1) * 5
            rafDiscountCodes.append(DiscountCode(value: Double(randomInt),
                                                 currencySymbol: "$",
                                                 code: randomString))
        }
        
        var dynamicItems = [DynamicItem]()
        dynamicItems.append(DynamicItem(title: "LEVO I",
                                        description: "The streamlined LEVO infusion process you know and love, with an abundance of improved features and innovative new technology.",
                                        imageUrl: "https://cdn.shopify.com/s/files/1/1362/6597/products/LEVO-II-Robin_200x.png?v=1550264399)",
                                        actionUrl: "https://shop.levooil.com/products/levo-oil-infuser",
                                        auxiliaryText: "$20"))
        
        dynamicItems.append(DynamicItem(title: "LEVO II",
                                        description: "LEVO I allows you to infuse oil and butter with the flavors and nutrients of herbs, fruits, and other ingredients at the touch of a button.",
                                        imageUrl: "https://cdn.shopify.com/s/files/1/1362/6597/products/LEVO-I-Terracotta_200x.png?v=1550266985",
                                        actionUrl: "https://shop.levooil.com/products/levo-ii",
                                        auxiliaryText: "$1400"))
        
        dynamicItems.append(DynamicItem(title: "Power Pod",
                                        description: "This double-sized Herb Pod is designed to fit perfectly inside our updated, slanted reservoir, increasing the strength of your infusions in one easy step.",
                                        imageUrl: "https://cdn.shopify.com/s/files/1/1362/6597/products/imageedit_27_4448118701_grande.png?v=1558727120",
                                        actionUrl: "https://shop.levooil.com/collections/accessories/products/power-pod",
                                        auxiliaryText: "$651"))
        
        let dynamicData = !hasDynamicData ? nil : DynamicData(title: "See what you can get",
                                                              dynamicItems: dynamicItems)
        
        let pendingJobs = [RafJob]()
        
        return RafData(staticData: rafStaticData,
                       rafProgram: rafProgram,
                       altruisticProgram: altruisticProgram,
                       rewards: hasRewards ? rafRewards : [Reward](),
                       discountCodes: hasCredit ? rafDiscountCodes : [DiscountCode](),
                       dynamicData: dynamicData,
                       pendingJobs: pendingJobs)
        
    }
    
    private static func generateRandomString(_ n: Int) -> String {
        let a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        var s = ""
        
        for _ in 0..<n {
            let r = Int(arc4random_uniform(UInt32(a.count)))
            s += String(a[a.index(a.startIndex, offsetBy: r)])
        }
        
        return s
    }
    
}

//
//  JobStatusResponse.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 27/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

public class JobStatusResponse {
    
    private enum Keys {
        static let jobKey = "job"
        static let rafDataKey = "rafData"
    }
    
    public let job: RafJobResponse
    public let rafDataResponse: RafDataResponse?
    
    // MARK: - Init
    
    init?(withDictionary dictionary: [String: Any]) {
        guard let jobDict = dictionary[Keys.jobKey] as? [String:Any],
            let job = RafJobResponse(withDictionary: jobDict) else {
                return nil
        }
        
        self.job = job
        
        var rafDataResponse: RafDataResponse? = nil
        if let rafDataDict = dictionary[Keys.rafDataKey] as? [String:Any] {
            rafDataResponse  = RafDataResponse(withDictionary: rafDataDict)
        }
        self.rafDataResponse = rafDataResponse
    }
}

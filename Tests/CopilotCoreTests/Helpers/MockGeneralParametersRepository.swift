//
//  MockGeneralParametersRepository.swift
//  CopilotAPIAccessTests
//
//  Created by Revital Pisman on 04/11/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

class MockGeneralParametersRepository: GeneralParametersRepository {
    var generalParameters: [String: String]
    var generalParametersKeys: [String] {
        return Array(generalParameters.keys) 
    }
    
    init(_ params: [String : String]) {
        self.generalParameters = params
    }
}

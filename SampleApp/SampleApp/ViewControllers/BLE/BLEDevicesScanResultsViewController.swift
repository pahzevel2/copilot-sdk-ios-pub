//
//  BLEDevicesScanResultsViewController.swift
//  SampleApp
//
//  Created by Adaya on 11/01/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import UIKit
import ZemingoBLELayer
import CopilotAPIAccess

class BLEDevicesScanResultsViewController: UIViewController , UITableViewDataSource, UITableViewDelegate, BLEPeripheralDiscovererDelegate{
   
    @IBOutlet weak var scannedDevicesTable: UITableView!
    var scanner : BLEDiscovererProtocol?
    var bridge : BLEBridge?
    var results : [PeripheralInfo   ] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scannedDevicesTable.dataSource = self
        scannedDevicesTable.delegate = self

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let bleBridge = bridge{
            scanner = bleBridge.createBLEScanner(with: self)
            let error = scanner?.scanForBLEPeripherals()
            if error != nil{
                 DispatchQueue.main.async{
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let blesScanner = scanner{
            let error = blesScanner.scanForBLEPeripherals()
            if error != nil{
                DispatchQueue.main.async{
                    self.showAlert("Error starting scanner \(error!)")
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let blesScanner = scanner{
            blesScanner.stopScan()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func blePeripheralFound(with peripheralInfo: PeripheralInfo!) {
        DispatchQueue.main.async{
            self.results.append(peripheralInfo)
            self.scannedDevicesTable.reloadData()
        }
    }
    
   
    
    func blePeripheralScanStoppedUnexpectedWithError(_ error: Error!) {
        DispatchQueue.main.async{
            self.showAlert("scan stopped unexecpectly")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "peripheralSelected", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = scannedDevicesTable.dequeueReusableCell(withIdentifier: "scannedDevice", for: indexPath)
        let peripheralInfo = results[indexPath.row]
        cell.detailTextLabel?.text = peripheralInfo.identifier.description
        cell.textLabel?.text = peripheralInfo.name
        return cell
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "peripheralSelected"{
            let indexPath = sender as! IndexPath
            let peripheralInfo = results[indexPath.row]
            let controller = segue.destination as! BLEPeripheralViewController
            controller.bridge = self.bridge
            controller.peripheralInfo = peripheralInfo
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}

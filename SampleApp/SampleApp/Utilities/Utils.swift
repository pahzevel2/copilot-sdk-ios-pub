//
//  Utils.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

struct Utils {

    static func getAppVersion(shouldConcatinateBuildNumber: Bool) -> String? {
        
        var versionString: String?
        
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            if shouldConcatinateBuildNumber {
                if let buildVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                    versionString = appVersion + "_" + buildVersion
                }
                else {
                    //Do nothing - This will return nil (failure)
                }
            }
            else {
                versionString = appVersion
            }
        }
        return versionString
    }
}

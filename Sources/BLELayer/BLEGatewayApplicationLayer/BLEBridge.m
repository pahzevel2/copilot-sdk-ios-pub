//
//  BLEBridge.m
//  ZemingoBLELayer
//
//  Created by Ofir Zucker on 27/06/2017.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import "BLEBridge.h"
#import "BLEDiscovererProtocol.h"
#import "BLEStatusDelegateProtocol.h"
#import "CommandFlowProtocol.h"
#import "BLECommunicationFactory.h"
#import "Peripheral.h"
#import "PeripheralInfo.h"


@interface BLEBridge()

@property (nonatomic, strong)NSArray *discoveryServices;
@property (nonatomic, strong)NSArray *requiredServices;

@end

@implementation BLEBridge

- (instancetype)initWithRequiredServices:(NSArray *)requiredServices discoveryServices:(NSArray *)discoveryServices
{
    self = [super init];
    if (self) {
        self.requiredServices = requiredServices;
        self.discoveryServices = discoveryServices;
    }
    return self;
}

#pragma mark - public

- (id<BLEDiscovererProtocol>)createBLEScannerWithDelegate:(id<BLEPeripheralDiscovererDelegate>)delegate {
    return  [[BLECommunicationFactory sharedManager] createBleDiscovererWithDiscoveryServices:self.discoveryServices delegate:delegate];
}

- (Peripheral *)connectToPeripheralWithPeripheralInfo:(PeripheralInfo *) peripheralInfo perhiperalConnectionDelegate:(id<PeripheralConnectionDelegate>)delegate {
 
    
    Peripheral *peripheral = [[Peripheral alloc] initWithPeripheralInfo: peripheralInfo requiredServices:self.requiredServices connectionDelegate:delegate];
    return peripheral;
}

//status delegates

- (void)addStatusDelegate:(id<BLEStatusDelegate>) delegate {
    [[BLECommunicationFactory sharedManager] addStatusDelelate:delegate];
}

- (void)removeStatusDelegate:(id<BLEStatusDelegate>) delegate {
    [[BLECommunicationFactory sharedManager] removeStatusDelegate:delegate];
}

- (BOOL) isBLEOn {
    return [[BLECommunicationFactory sharedManager] isBLEOn];
}

@end

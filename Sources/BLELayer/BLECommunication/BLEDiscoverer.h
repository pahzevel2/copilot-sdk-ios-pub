//
//  BleDiscoverer.h
//  ZemingoBLELayer
//
//  Created by Miko Halevi on 7/10/17.
//  Copyright © 2017 Adaya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEDiscovererProtocol.h"
#import "BleDiscovererControl.h"
#import <CoreBluetooth/CoreBluetooth.h>

@class PeripheralInfo;

@interface BLEDiscoverer : NSObject <BLEDiscovererProtocol>

- (instancetype)initWithDiscovererControl:(id<BleDiscovererControl>) discovererControl discovererDelegate:(id<BLEPeripheralDiscovererDelegate>)discovererDelegate discoveryServices: (NSArray *)discoveryServices;

- (void)didDiscoverPeripheralWithPeripheralInfo:(PeripheralInfo *)peripheralInfo;
- (void)bleSuspendedWitherror:(NSError *)error;

@end

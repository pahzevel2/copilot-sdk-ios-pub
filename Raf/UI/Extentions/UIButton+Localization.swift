//
//  UIButton+Localization.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 13/08/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

extension UIButton {
    
    @IBInspectable var localizeKey: String {
        set {
            let text = NSLocalizedString(newValue, bundle: Bundle(for: Copilot.self), comment:"");
            setTitle(text, for: .normal)
        }
        get {
            return ""
        }
    }
    
}

//
//  CustomAnalyticsEvents.swift
//  SampleApp
//
//  Created by Tom Milberg on 09/04/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess

struct CustomAnalyticsEvent: AnalyticsEvent {
    
    let customFieldName1: String
    let customFieldName2: String
    let screenName: String
    
    var customParams: Dictionary<String, String> {
        return ["customFieldName1" : customFieldName1, "customFieldName2" : customFieldName2, "screenName" : screenName]
    }
    
    var eventName: String {
        return "Custom_Event"
    }

    var eventOrigin: AnalyticsEventOrigin {
        return .App
    }
    
    var eventGroups: [AnalyticsEventGroup] {
        return [.All, .Main]
    }
}

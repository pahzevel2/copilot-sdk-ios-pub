//
//  ServerErrorTest.swift
//  CopilotAuthTests
//
//  Created by Adaya on 03/12/2017.
//  Copyright © 2017 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class ServerErrorTests: XCTestCase {
    static let reasonKey = "reason"
    static let errorMessage = "errorMessage"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
     func serverErrorFromStatusCode(_ statusCode: Int, error: String?,  reason: String?)->ServerError?{
        var json:[String:Any] = [:]
        if let reason1 = reason {json[ServerErrorTests.reasonKey] = reason1}
        if let error1 = error {json[ServerErrorTests.errorMessage] = error1}
        return ServerError.serverErrorFromStatusCode(statusCode, json)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    
    
    func testSuccess(){
        let error = serverErrorFromStatusCode(200, error: "a message", reason: "a message")
        XCTAssertNil(error)
    }
    
    func testAllSuccess(){
        for i in 200..<400 {
            let error = serverErrorFromStatusCode(i, error: "a message", reason: "a message")
            XCTAssertNil(error, "\(i) should match success")
        }
    }
    
    func testAllWithoutBodyUnknown(){
        for i in 400..<900 {
            let error = ServerError.serverErrorFromStatusCode(i, nil)
            XCTAssertNotNil(error)
            XCTAssertEqual(error!, ServerError.unknown(statusCode: i, message: "No additional information provided"))
        }
    }
    
    func testAllWithoutReasonUnknown(){
        for i in 400..<900 {
            let error = serverErrorFromStatusCode(i, error: "an error", reason: nil)
            XCTAssertNotNil(error)
            XCTAssertEqual(error!, ServerError.unknown(statusCode: i, message: "an error"))
        }
    }
    
    func testAllWithoutReasonOrErrorUnknown(){
        for i in 400..<900 {
            let error = serverErrorFromStatusCode(i, error: nil, reason: nil)
            XCTAssertNotNil(error)
            XCTAssertEqual(error!, ServerError.unknown(statusCode: i, message: "No additional information provided"))
        }
    }
    
    func testUnauthorized(){
        let error = serverErrorFromStatusCode(401, error: nil, reason:"auth.unauthorized")
        XCTAssertNotNil(error)
        XCTAssertEqual(error!, ServerError.authorizationFailure)
    }
    
    func testInternalServerErrorWOMessage(){
        let error = serverErrorFromStatusCode(500, error: nil, reason:"util.internalError")
        XCTAssertNotNil(error)
        XCTAssertEqual(error!, ServerError.internalServerError(message: "No additional information provided"))
    }
    func testInternalServerError(){
        let error = serverErrorFromStatusCode(500, error: "any", reason:"util.internalError")
        XCTAssertNotNil(error)
        XCTAssertEqual(error!, ServerError.internalServerError(message: "any"))
    }
    
    func testInvalidServerErrorWOMessage(){
        let error = serverErrorFromStatusCode(400, error: nil, reason:"common.missingFields")
        XCTAssertNotNil(error)
        XCTAssertEqual(error!, ServerError.validationError(message: "No additional information provided"))
    }
    func testInvalidServerError(){
        let error = serverErrorFromStatusCode(400, error: "any", reason:"common.missingFields")
        XCTAssertNotNil(error)
        XCTAssertEqual(error!, ServerError.validationError(message: "any"))
    }
    
    func otherCopilotErrorWOMessage(){
        for i in 402..<500 {
            let error = serverErrorFromStatusCode(i, error: nil, reason: "any")
            XCTAssertNotNil(error)
            XCTAssertEqual(error!, ServerError.copilotError(statusCode: i, reason: "any", message: "No additional information provided"))
        }
        for i in 501..<900 {
            let error = serverErrorFromStatusCode(i, error: nil, reason: "any")
            XCTAssertNotNil(error)
            XCTAssertEqual(error!, ServerError.copilotError(statusCode: i, reason: "any", message: "No additional information provided"))
        }
    }
    
    func otherCopilotError(){
        for i in 402..<500 {
            let error = serverErrorFromStatusCode(i, error: "a", reason: "any")
            XCTAssertNotNil(error)
            XCTAssertEqual(error!, ServerError.copilotError(statusCode: i, reason: "any", message: "a"))
        }
        for i in 501..<900 {
            let error = serverErrorFromStatusCode(i, error: "a", reason: "any")
            XCTAssertNotNil(error)
            XCTAssertEqual(error!, ServerError.copilotError(statusCode: i, reason: "any", message: "a"))
        }
    }

   
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
  
}

extension ServerError : Equatable{
    public static func ==(lhs: ServerError, rhs: ServerError) -> Bool {
        switch (lhs, rhs) {
        case (.authorizationFailure, .authorizationFailure):
            return true
        case (.validationError(let message), .validationError(let message2)):
            return message == message2
        case (.copilotError(let statusCode, let reason, let message), .copilotError(let statusCode2, let reason2, let message2)):
            return message == message2 && statusCode == statusCode2 && reason == reason2
        case (.communication(_), .communication(_)):
            return true
        case (.generalError(let message), .generalError(let message2)):
            return message == message2
        case (.internalServerError(let message), .internalServerError(let message2)):
            return message == message2
        case (.unknown(let statusCode, let message), .unknown(let statusCode2, let message2)):
            return message == message2 && statusCode == statusCode2
        default:
            return false
        }
    }
    
    
    
    
}

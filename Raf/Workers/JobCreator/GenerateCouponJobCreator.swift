//
//  GenerateCouponJobCreator.swift
//  CopilotAPIAccess
//
//  Created by Revital Pisman on 02/09/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

class GenerateCouponJobCreator: JobCreator<GenerateReferralCouponError> {
    
    typealias Dependencies = HasRafServiceInteraction
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    override func createJob(_ closure: @escaping JobCreatorClosure<GenerateReferralCouponError>) {
        dependencies.rafServiceInteraction.generateReferralCoupon(generateReferralCouponClosure: closure)
    }
    
    override func getGeneralError(withDebugMessage debugMessage: String) -> GenerateReferralCouponError {
        return GenerateReferralCouponError.generalError(debugMessage: debugMessage)
    }
    
    override func mapPollingError(withGetJobStatusError getJobStatusError: GetJobStatusError) -> GenerateReferralCouponError {
        let error: GenerateReferralCouponError
        
        switch getJobStatusError {
        case .requiresRelogin(let debugMessage):
            error = GenerateReferralCouponError.requiresRelogin(debugMessage: debugMessage)
        case .generalError(let debugMessage):
            error = GenerateReferralCouponError.generalError(debugMessage: debugMessage)
        case .connectivityError(let debugMessage):
            error = GenerateReferralCouponError.connectivityError(debugMessage: debugMessage)
        }
        
        return error
    }
    
}

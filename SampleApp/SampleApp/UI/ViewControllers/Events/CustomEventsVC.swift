//
//  CustomEventsVC.swift
//  SampleApp
//
//  Created by Revital Pisman on 14/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit
import CopilotAPIAccess

class CustomEventsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Custom Events"
    }
    
    @IBAction func customEvent1ButtonPressed(_ sender: Any) {
        let customEvent1 = CustomAnalyticsEvent(customFieldName1: "Custom Event 1", customFieldName2: "Custom Field Name 2", screenName: "Custom Events")
        Copilot.instance.report.log(event: customEvent1)
    }
    
    @IBAction func customEvent2ButtonPressed(_ sender: Any) {
        let customEvent2 = CustomAnalyticsEvent2(smartDeviceID: "1234567890")
        Copilot.instance.report.log(event: customEvent2)
    }
    
}

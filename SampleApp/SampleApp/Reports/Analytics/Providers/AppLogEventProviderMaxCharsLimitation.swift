//
//  AppLogEventProviderMaxCharsLimitation.swift
//  SampleApp
//
//  Created by Tom Milberg on 09/04/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import Foundation
import CopilotAPIAccess
import CopilotLogger

class AppLogEventProviderMaxCharsLimitation: EventLogProvider {
    
    let maxValueLength: Int
    
    init(maxValueLength: Int) {
        self.maxValueLength = maxValueLength
    }
    
    func enable() {
        ZLogManagerWrapper.sharedInstance.shouldWriteToFile(true)
    }
    
    func disable() {
        ZLogManagerWrapper.sharedInstance.shouldWriteToFile(false)
    }

    func setUserId(userId: String?){
        // Do nothing
    }

    func transformParameters(parameters: Dictionary<String, String>) -> Dictionary<String, String> {
        
        var transformedParameters = parameters
        
        parameters.forEach { (key, value) in
            if (value.count > maxValueLength) {
                let maxLengthIndex = value.index(value.startIndex, offsetBy: maxValueLength)
                let newValue = value.substring(to: maxLengthIndex)
                
                ZLogManagerWrapper.sharedInstance.logInfo(message: "Transforming value: \(value) to: \(newValue) due to max value length validation")
                
                transformedParameters.updateValue(newValue, forKey: key)
            }
        }
        
        return transformedParameters
    }
    
    func logCustomEvent(eventName: String, transformedParams: Dictionary<String, String>) {
        
        ZLogManagerWrapper.sharedInstance.logInfo(message: "Reporting custom event with event name: \(eventName)")
        
        transformedParams.forEach { (key, value) in
            ZLogManagerWrapper.sharedInstance.logInfo(message: "Reporting param key \(key) and value \(value)")
        }
    }
    
    var providerName: String {
        return "ProviderWithMaxCharsLimitation"
    }
    
    var providerEventGroups: [AnalyticsEventGroup] {
        return [.DevSupport]
    }
}

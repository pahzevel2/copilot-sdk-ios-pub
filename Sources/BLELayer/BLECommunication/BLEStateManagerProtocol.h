//
//  BLEStateManager.h
//  FlirNvrDvr
//
//  Created by Elad Urson on 25/1/16.
//  Copyright © 2016 Zemingo. All rights reserved.
//


@protocol BLEStateManagerDelegate <NSObject>

//- (void) BLEStateChangedTo:(BOOL)isBLEOn;

@end


@protocol BLEStateManagerProtocol <NSObject>

//There is no delegate because changing the state of the BT is done when the app is not active, and not during normal runtime.
//@property (nonatomic, weak) id <BLEStateManagerDelegate> stateDelegate;

- (BOOL) isBLEOn; //should be called when the app becomes active

@end

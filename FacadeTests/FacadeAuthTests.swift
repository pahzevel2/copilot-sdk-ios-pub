//
//  FacadeAuthTests.swift
//  CopilotAPIAccessTests
//
//  Created by Ofer Meroz on 18/11/2018.
//  Copyright © 2018 Zemingo. All rights reserved.
//

import XCTest
@testable import CopilotAPIAccess

class FacadeAuthTests: XCTestCase {
    
    private var authAPI: AuthAPIAccess!
    
    override func setUp() {
        super.setUp()
        
        let dependencies = TestAuthDependencies(userServiceInteraction: AuthTestsUserServiceInteraction(), authenticationServiceInteraction: AuthTestsAuthenticationServiceInteraction(), reporter: DummyReportAPI())
        authAPI = AuthAPI(dependencies: dependencies)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // MARK: - Tests
    
    func testLoginWithEmailPassword() {
        let expectation = self.expectation(description: "login call closure should be executed")
        
        authAPI
            .login()
            .with(email: testEmailAddress, password: testPassword)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testLoginSilently() {
        let expectation = self.expectation(description: "login call closure should be executed")
        
        authAPI
            .login()
            .silently
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testSignUpAnonymously() {
        let expectation = self.expectation(description: "registerAnonymously call closure should be executed")
        
        authAPI
            .signup()
            .withCopilotAnalysisConsent(true)
            .withCustomConsent(testConsentName, value: true)
            .anonymously
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testSignUpWithEmailPassword() {
        let expectation = self.expectation(description: "register call closure should be executed")
        
        authAPI
            .signup()
            .withCopilotAnalysisConsent(true)
            .withCustomConsent(testConsentName, value: true)
            .with(email: testEmailAddress, password: testPassword, firstname: testFirstName, lastname: testLastName)
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testLogout() {
        let expectation = self.expectation(description: "logout call closure should be executed")
        
        authAPI
            .logout()
            .build()
            .execute { (_) in
                expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30, handler: nil)
    }
    
}

// MARK: - Helpers

fileprivate struct TestAuthDependencies: HasUserServiceInteraction, HasAuthenticationServiceInteraction, HasReporter {
    let userServiceInteraction: UserServiceInteractable
    let authenticationServiceInteraction: AuthenticationServiceInteractable
    let reporter: ReportAPIAccess
}

fileprivate class AuthTestsUserServiceInteraction: BaseTestUserServiceInteraction {
    
    override func me(getCurrentUserClosure: @escaping GetCurrentUserClosure) {
        let res: Response<UserMe, FetchMeError> = .success(testUser)
        getCurrentUserClosure(res)
    }
}

fileprivate class AuthTestsAuthenticationServiceInteraction: BaseTestAuthenticationServiceInteraction {
    
    override func login(withEmail email: String, password: String, loginClosure: @escaping LoginClosure) {
        XCTAssertEqual(email, testEmailAddress)
        XCTAssertEqual(password, testPassword)
        let res: Response<Void, LoginError> = .success(())
        loginClosure(res)
    }
    
    override func attemptToSilentLogin(closure: @escaping SilentLoginClosure) {
        let res: Response<Void, LoginSilentlyError> = .success(())
        closure(res)
    }
    
    override func registerAnonymously(withConsents consents: [String : Bool], registerClosure: @escaping RegisterAnonymouslyClosure) {
        XCTAssertEqual(consents, testConsent)
        let res: Response<Void, SignupAnonymouslyError> = .success(())
        registerClosure(res)
    }
    
    override func register(withEmail email: String, password: String, firstName: String, lastName: String, consents: [String : Bool], registerClosure: @escaping RegisterClosure) {
        XCTAssertEqual(email, testEmailAddress)
        XCTAssertEqual(password, testPassword)
        XCTAssertEqual(firstName, testFirstName)
        XCTAssertEqual(lastName, testLastName)
        XCTAssertEqual(consents, testConsent)
        let res: Response<Void, SignupError> = .success(())
        registerClosure(res)
    }
    
    override func logout(logoutClosure: (Response<Void, LogoutError>) -> Void) {
        let res: Response<Void, LogoutError> = .success(())
        logoutClosure(res)
    }
}

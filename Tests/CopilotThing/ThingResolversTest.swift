//
//  ThingResolversTest.swift
//  CopilotAPIAccessTests
//
//  Created by Adaya on 05/03/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation
import XCTest
@testable import CopilotAPIAccess

class UpdateThingErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = UpdateThingError
    typealias R = UpdateThingErrorResolver
    
    let msg = "msg"
    
    func resolver() -> UpdateThingErrorResolver {
        return UpdateThingErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customEntityNotFound() -> Bool {
        return true
    }
    
    func test_EntityNotFound(){
        let error = resolver().fromTypeSpecificError(TestConstants.entityNotFound, TestConstants.entityNotFoundReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .thingNotFound(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func customForbidden() -> Bool {
        return true
    }
    
    func test_fobidden(){
        let error = resolver().fromTypeSpecificError(TestConstants.fobidden, TestConstants.forbiddenReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .thingIsNotAssociated(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
}

class FetchSingleThingErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = FetchSingleThingError
    typealias R = FetchSingleThingErrorResolver
    
    let msg = "msg"
    
    func resolver() -> FetchSingleThingErrorResolver {
        return FetchSingleThingErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customEntityNotFound() -> Bool {
        return true
    }
    
    func test_EntityNotFound(){
        let error = resolver().fromTypeSpecificError(TestConstants.entityNotFound, TestConstants.entityNotFoundReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .thingNotFound(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func customForbidden() -> Bool {
        return true
    }
    
    func test_fobidden(){
        let error = resolver().fromTypeSpecificError(TestConstants.fobidden, TestConstants.forbiddenReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .thingIsNotAssociated(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
}

class AssociateThingErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = AssociateThingError
    typealias R = AssociateThingErrorResolver
    
    let msg = "msg"
    
    func resolver() -> AssociateThingErrorResolver {
        return AssociateThingErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
  
    func test_AlreadyAssociated(){
        let error = resolver().fromTypeSpecificError(TestConstants.fobidden, "associationFailure.thingAlreadyAssociated", msg)
        XCTAssertNotNil(error)
        switch error! {
        case .thingAlreadyAssociated(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func test_AlreadyAssociatedWOStatus(){
        let error = resolver().fromTypeSpecificError(1, "associationFailure.thingAlreadyAssociated", "any")
        XCTAssertNil(error)
    }
    
    func test_ThingNotAllowed(){
        let error = resolver().fromTypeSpecificError(TestConstants.fobidden, "associationFailure.thingNotAllowed", msg)
        XCTAssertNotNil(error)
        switch error! {
        case .thingNotAllowed(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
    
    func test_ThingNotAllowedWOStatus(){
        let error = resolver().fromTypeSpecificError(1, "associationFailure.thingNotAllowed", "any")
        XCTAssertNil(error)
    }
}

class DisassociateThingErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = DisassociateThingError
    typealias R = DisassociateThingErrorResolver
    
    let msg = "msg"
    
    func resolver() -> DisassociateThingErrorResolver {
        return DisassociateThingErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
    
    func customEntityNotFound() -> Bool {
        return true
    }
    
    func test_EntityNotFound(){
        let error = resolver().fromTypeSpecificError(TestConstants.entityNotFound, TestConstants.entityNotFoundReason, msg)
        XCTAssertNotNil(error)
        switch error! {
        case .thingNotFound(debugMessage: msg):break
        default: XCTFail("Received \(error!)")
        }
    }
}

class CanAssociateThingErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = CanAssociateThingError
    typealias R = CanAssociateThingErrorResolver
    
    let msg = "msg"
    
    func resolver() -> CanAssociateThingErrorResolver {
        return CanAssociateThingErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .invalidParameters(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
}

class FetchThingsErrorResolverTests: XCTestCase, ResolverTest {
    
    typealias T = FetchThingsError
    typealias R = FetchThingsErrorResolver
    
    let msg = "msg"
    
    func resolver() -> FetchThingsErrorResolver {
        return FetchThingsErrorResolver()
    }
    
    func test_connectivity(){
        let error = resolver().fromConnectivityError(debugMessage: msg)
        switch error {
        case .connectivityError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_general(){
        let error = resolver().fromGeneralError(debugMessage: msg)
        switch error {
        case .generalError(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    func test_invalidParameters(){
        let error = resolver().fromInvalidParametersError(debugMessage: msg)
        switch error {
        case .generalError(_):break
        default: XCTFail("Received \(error)")
        }
    }
    func test_requiresRelogin(){
        let error = resolver().fromRequiresReloginError(debugMessage: msg)
        switch error {
        case .requiresRelogin(debugMessage: msg):break
        default: XCTFail("Received \(error)")
        }
    }
    
    
    func test_Unrecognized(){
        assertUnrecognized()
    }
}
